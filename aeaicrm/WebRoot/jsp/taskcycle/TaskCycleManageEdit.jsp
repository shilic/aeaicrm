<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>周期定义管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>开始日期</th>
	<td><input id="TC_BEGIN" label="开始日期" name="TC_BEGIN" type="text" value="<%=pageBean.inputValue("TC_BEGIN")%>" size="24" class="text" readonly="readonly"/>
	<img id="TC_BEGINPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>结束日期</th>
	<td><input id="TC_END" label="结束日期" name="TC_END" type="text" value="<%=pageBean.inputValue("TC_END")%>" size="24" class="text" readonly="readonly"/>
	<img id="TC_ENDPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="TC_ID" name="TC_ID" value="<%=pageBean.inputValue4DetailOrUpdate("TC_ID","")%>" />
</form>
<script language="javascript">
initDetailOpertionImage();
initCalendar('TC_BEGIN','%Y-%m-%d','TC_BEGINPicker');
initCalendar('TC_END','%Y-%m-%d','TC_ENDPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("TC_BEGIN");
datetimeValidators[0].set("yyyy-MM-dd").add("TC_END");
requiredValidator.add("TC_BEGIN");
requiredValidator.add("TC_END");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
