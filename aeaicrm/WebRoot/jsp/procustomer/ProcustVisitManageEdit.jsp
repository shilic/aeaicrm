<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户拜访记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<!--    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td> -->
<!--    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td> -->
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>拜访类别</th>
    <td><input id="PROCUST_VISIT_TYPE_TEXT" label="拜访类型" name="PROCUST_VISIT_TYPE_TEXT" type="text" value="<%=pageBean.selectedText("CUST_VISIT_CATEGORY")%>" size="24"  class="text" readonly="readonly"/>
	<input id="CUST_VISIT_CATEGORY" label="拜访类型" name="CUST_VISIT_CATEGORY" type="hidden" value="<%=pageBean.selectedValue("CUST_VISIT_CATEGORY")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访日期</th>
	<td><input id="PROCUST_VISIT_DATE" label="拜访日期" name="PROCUST_VISIT_DATE" type="text" value="<%=pageBean.inputDate("PROCUST_VISIT_DATE")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访类型</th>
    <td><input id="PROCUST_VISIT_TYPE_TEXT" label="拜访类型" name="PROCUST_VISIT_TYPE_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_TYPE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_TYPE" label="拜访类型" name="PROCUST_VISIT_TYPE" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_TYPE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访效果</th>
    <td><input id="PROCUST_VISIT_EFFECT_TEXT" label="拜访效果" name="PROCUST_VISIT_EFFECT_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_EFFECT")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_EFFECT" label="拜访效果" name="PROCUST_VISIT_EFFECT" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_EFFECT")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input id="PROCUST_VISIT_FILL_NAME" label="填写人" name="PROCUST_VISIT_FILL_NAME" type="text" value="<%=pageBean.inputValue("PROCUST_VISIT_FILL_NAME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>填写时间</th>
	<td><input id="PROCUST_VISIT_FILL_TIME" label="填写时间" name="PROCUST_VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("PROCUST_VISIT_FILL_TIME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访备注</th>
	<td><textarea id="PROCUST_VISIT_REMARK" label="拜访备注" name="PROCUST_VISIT_REMARK" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_REMARK")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td><textarea id="PROCUST_VISIT_CUST_FOCUS" label="客户关注点" name="PROCUST_VISIT_CUST_FOCUS" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="PROCUST_VISIT_ID" name="PROCUST_VISIT_ID" value="<%=pageBean.inputValue4DetailOrUpdate("PROCUST_VISIT_ID","")%>" />
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
</form>
<script language="javascript">
datetimeValidators[0].set("yyyy-MM-dd").add("PROCUST_VISIT_DATE");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("PROCUST_VISIT_FILL_TIME");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
