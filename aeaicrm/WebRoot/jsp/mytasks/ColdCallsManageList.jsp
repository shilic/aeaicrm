<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>陌生拜访</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var orgIdBox;
function openProIdBox(){
	var handlerId = "ProCustomerSelectList"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择潜在客户',{size:'normal',width:'500',top:'2px'});
	}
	var reviewId = $('#TASK_REVIEW_ID').val();
	var url = 'index?'+handlerId+'&targetId=ORG_ID&TASK_REVIEW_ID='+reviewId;
	orgIdBox.sendRequest(url);
}
function createTask(){
	$('#ids').val();
	$('#TASK_REVIEW_ID').val();
	doSubmit({actionType:'createTask'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
<%if (!"ConfirmSummary".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="openProIdBox()"><input value="&nbsp;" title="创建" type="button" class="createImgBtn" />创建</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
<%} else{%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
<%} %>
<%if ("Init".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
<%} %>     
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="陌生拜访.csv"
retrieveRowsCallback="process" xlsFileName="陌生拜访.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{TASK_ID:'${row.TASK_ID}'});refreshConextmenu()" onclick="selectRow(this,{TASK_ID:'${row.TASK_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="120" property="ORG_NAME" title="客户名称" />
	<ec:column width="80" property="ORG_CLASSIFICATION" title="分类" mappingItem="ORG_CLASSIFICATION"/>
	<ec:column width="50" property="ORG_STATE" title="状态" mappingItem="ORG_STATE"/>
	<ec:column width="80" property="ORG_CREATER_NAME" title="创建人" />
	<ec:column width="100" property="ORG_CREATE_TIME" title="创建时间" />
    <ec:column width="100" property="ORG_UPDATE_TIME" title="更新时间" />
	<ec:column width="120" property="ORG_LABELS_NAME" title="标签" />
	<ec:column width="120" property="TASK_FOLLOW_STATE" title="跟进状态" mappingItem="TASK_FOLLOW_STATE" />
</ec:row>
</ec:table>
<input type="hidden" name="TASK_ID" id="TASK_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="ORG_ID" id="ORG_ID" value="" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
<input type="hidden" name="ids" id="ids" value="<%=pageBean.inputValue("ids")%>" />
<input type="hidden" name="TASK_REVIEW_STATE" id="TASK_REVIEW_STATE" value="<%=pageBean.inputValue("TASK_REVIEW_STATE")%>" />
<script language="JavaScript">
setRsIdTag('TASK_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
