<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField,tableMode){
	if ('insert' != operaType && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',top:'2px'});
	}
	var columnIdValue = "";
	if ('Many2ManyAndRel'==tableMode){
		columnIdValue = $("#curColumnId").val();
		if ('insert' == operaType){
			columnIdValue = $("#columnId").val();
		}
	}
	else{
		columnIdValue = $("#columnId").val();	
	}
	var url = 'index?'+handlerId+'&GRP_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);	
}
function doRemoveContent(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要移除该条记录吗？',function(r){
		if(r){
			postRequest('form1',{actionType:'isLastRelation',onComplete:function(responseText){
				if (responseText == 'true'){
					jConfirm('该信息只有一条关联记录，确认要删除吗？',function(rr){
						if(rr){
							doSubmit({actionType:'delete'});
						}
					});
				}else{
					doSubmit({actionType:'removeContent'});
				}
			}});
		}
	});
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
var operaTreeBox;
function openTreeRequestBox(operaType){
	var title = "树节点管理";
	var handlerId = "CustomerGroupEdit";
	
	if ('insert' != operaType && !isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (!operaTreeBox){
		operaTreeBox = new PopupBox('operaTreeBox',title,{size:'normal',width:'500px',height:'360px',top:'2px'});
	}
	var url = '';
	if ('insert' == operaType){
		url = 'index?'+handlerId+'&operaType='+operaType+'&GRP_SUP_ID='+$("#columnId").val();
	}else{
		url = 'index?'+handlerId+'&operaType='+operaType+'&GRP_ID='+$("#columnId").val();
	}
	operaTreeBox.sendRequest(url);	
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
function deleteTreeNode(){
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	jConfirm('确认要删除该节点吗？',function(r){
		if(r){
			postRequest('form1',{actionType:'deleteTreeNode',onComplete:function(responseText){
				if (responseText == 'success'){
					$('#columnId').val("");
					doQuery();
				}else if (responseText == 'hasChild'){
					writeErrorMsg('该节点还有子节点，不能删除！');
				}else if (responseText == 'hasContent'){
					writeErrorMsg('有信息关联该分组，不能删除！');
				}
			}});	
		}
	});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	var columnIdValue = $("#columnId").val();
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (curAction == 'copyContent' || curAction == 'moveContent'){
		if (!isSelectedRow()){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		columnIdValue = $("#curColumnId").val()
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'2px'});
	}
	var handlerId = "CustomerGroupPick";
	var url = 'index?'+handlerId+'&GRP_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());		
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function moveRequest(moveAction){
	postRequest('form1',{actionType:moveAction,onComplete:function(responseText){
		if (responseText == 'success'){
			refreshTree();
		}else if (responseText == 'isFirstNode'){
			writeErrorMsg('该节点是同级第一个节点，不能上移！');
		}else if (responseText == 'isLastNode'){
			writeErrorMsg('该节点是同级最后一个节点，不能下移！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}
function changeTab(tabId){
	$('#_tabId_').val(tabId);
	refreshContent();
}
function saveTreeBaseRecord(){
	postRequest('form1',{actionType:'saveTreeBaseRecord',onComplete:function(responseText){
		if (responseText == 'success'){
			refreshTree();
		}else {
			writeErrorMsg('保存基本信息出错啦！');
		}
	}});
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild',checkUnique:'true'
	});
	}
	}
function checkInsertChild(){
	var result = true;
if (validation.checkNull($('#CHILD_GRP_CODE').val())){
	writeErrorMsg($("#CHILD_GRP_CODE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_GRP_CODE');
	return false;
}
if (validation.checkNull($('#CHILD_GRP_NAME').val())){
	writeErrorMsg($("#CHILD_GRP_NAME").attr("label")+"不能为空!");
	selectOrFocus('CHILD_GRP_NAME');
	return false;
}
if (validation.checkNull($('#CHILD_GRP_STATE').val())){
	writeErrorMsg($("#CHILD_GRP_STATE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_GRP_STATE');
	return false;
}
if ($('#CHILD_GRP_DESC').val().length > 128){
	writeErrorMsg($("#CHILD_GRP_DESC").attr("label")+"长度不能大于"+128+"!");
	selectOrFocus('CHILD_GRP_DESC');
	return false;
}
	return result;
}
function doCancel(){
	doRefresh($('#GRP_ID').val());
}
function doRefresh(nodeId){
	$('#GRP_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表</h3>        
        <div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />     
    </td>
	<td width="85%" valign="top">
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeTab('_base_')">基本信息</div>
<div class="newarticle1" hidden="hidden" onclick="changeTab('CustomerInfo')"></div>
</div>	
<div class="photobox newarticlebox" id="Layer<%=pageBean.inputValue("_tabIndex_")%>" style="height:auto;">
<%if ("_base_".equals(pageBean.inputValue("_tabId_"))){%>
<div id="__ToolBar__">
<table  id="_TreeToolBar_" border="0" cellpadding="0" cellspacing="1">
    <tr>
    <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveTreeBaseRecord()"><input value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td></aeai:previlege>
<%if (!pageBean.isTrue(pageBean.inputValue("isRootColumnId"))){%>
    <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="deleteTreeNode()"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td></aeai:previlege>
    <aeai:previlege code="transfer"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openTargetTreeBox('moveTree')"><input value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td></aeai:previlege>
    <aeai:previlege code="up"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="moveRequest('moveUp')"><input value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td></aeai:previlege>
    <aeai:previlege code="down"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="moveRequest('moveDown')"><input value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td></aeai:previlege>   
<%}%>
    </tr>
    </table>
</div>
<div style="margin:auto 2px;">
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>分组编码</th>
	<td><input id="GRP_CODE" label="GRP_CODE" name="GRP_CODE" type="text" value="<%=pageBean.inputValue("GRP_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组名称</th>
	<td><input id="GRP_NAME" label="GRP_NAME" name="GRP_NAME" type="text" value="<%=pageBean.inputValue("GRP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组状态</th>
	<td><select id="GRP_STATE" label="GRP_STATE" name="GRP_STATE" class="select"><%=pageBean.selectValue("GRP_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>分组描述</th>
	<td><textarea id="GRP_DESC" label="简要介绍" name="GRP_DESC" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("GRP_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" id="GRP_ID" name="GRP_ID" value="<%=pageBean.inputValue4DetailOrUpdate("GRP_ID","")%>" />
<input type="hidden" id="GRP_SUP_ID" name="GRP_SUP_ID" value="<%=pageBean.inputValue("GRP_SUP_ID")%>" />
<input type="hidden" id="GRP_SORT" name="GRP_SORT" value="<%=pageBean.inputValue("GRP_SORT")%>" />
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
</div>
<div style="margin-top: 10px;padding: 5px;">
	    <div id="__ToolBar__">
	<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		 <aeai:previlege code="createBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td></aeai:previlege>
		 <aeai:previlege code="cancelBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td></aeai:previlege>    
	    </tr></table>
	    </div>     
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>分组编码</th>
	<td><input id="CHILD_GRP_CODE" label="编码" name="CHILD_GRP_CODE" type="text" value="<%=pageBean.inputValue("CHILD_GRP_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组名称</th>
	<td><input id="CHILD_GRP_NAME" label="名称" name="CHILD_GRP_NAME" type="text" value="<%=pageBean.inputValue("CHILD_GRP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组状态</th>
	<td><select id="CHILD_GRP_STATE" label="GRP_STATE" name="CHILD_GRP_STATE" class="select"><%=pageBean.selectValue("CHILD_GRP_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>分组描述</th>
	<td><textarea id="CHILD_GRP_DESC" label="描述" name="CHILD_GRP_DESC" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("CHILD_GRP_DESC")%></textarea>
</td>
</tr>
	    </table>
	</div>
    <br />
</div>
<%}%>
<%if ("CustomerInfo".equals(pageBean.inputValue("_tabId_"))){%>
<div id="__ToolBar__">
<span style="float:right;height:28px;line-height:28px;"><input style="vertical-align:middle; margin-top:-2px; margin-bottom:1px;" name="showChildNodeRecords" type="checkbox" id="showChildNodeRecords" onclick="doQuery()" value="Y" <%=pageBean.checked(pageBean.inputValue("showChildNodeRecords"))%> />&nbsp;显示子节点记录&nbsp;</span>

</div>
<div style="margin:auto 2px;">
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="crm_customer_info.csv"
retrieveRowsCallback="process" xlsFileName="crm_customer_info.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();openContentRequestBox('detail','crm_customer_info','CustomerInfoEdit','CUST_ID','Many2ManyAndRel')" oncontextmenu="selectRow(this,{CUST_ID:'${row.CUST_ID}',curColumnId:'${row.GRP_ID}'});refreshConextmenu()" onclick="selectRow(this,{CUST_ID:'${row.CUST_ID}',curColumnId:'${row.GRP_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_NAME" title="CUST_NAME"   />
	<ec:column width="100" property="CUST_INDUSTRY" title="CUST_INDUSTRY"   />
	<ec:column width="100" property="CUST_PROVINCE" title="CUST_PROVINCE"   />
	<ec:column width="100" property="CUST_CITY" title="CUST_CITY"   />
	<ec:column width="100" property="CUST_ADDRESS" title="CUST_ADDRESS"   />
	<ec:column width="100" property="CUST_SCALE" title="CUST_SCALE"   />
	<ec:column width="100" property="CUST_NATURE" title="CUST_NATURE"   />
	<ec:column width="100" property="CUST_INTRODUCE" title="CUST_INTRODUCE"   />
	<ec:column width="100" property="CUST_STATE" title="CUST_STATE"   />
	<ec:column width="100" property="CUST_CREATE_ID" title="CUST_CREATE_ID"   />
	<ec:column width="100" property="CUST_CREATE_TIME" title="CUST_CREATE_TIME" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="CUST_SUBMIT_ID" title="CUST_SUBMIT_ID"   />
	<ec:column width="100" property="CUST_SUBMIT_TIME" title="CUST_SUBMIT_TIME" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="CUST_CONFIRM_ID" title="CUST_CONFIRM_ID"   />
	<ec:column width="100" property="CUST_CONFIRM_TIME" title="CUST_CONFIRM_TIME" cell="date" format="yyyy-MM-dd HH:mm" />
</ec:row>
</ec:table>
<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;display:none; z-index:10; width:480px;">
<b class="b9"></b>
<div class="content">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
</table>
<div style="width:100%;text-align:center;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="CUST_ID" id="CUST_ID" value="" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<script language="JavaScript">
setRsIdTag('CUST_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</div>
<%}%>
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<script language="javascript">
$('#GRP_DESC').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#CHILD_GRP_DESC').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("_tabIndex_")%>);
$(function(){
	resetTreeHeight(80);	
	resetTabHeight(80);
});
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
