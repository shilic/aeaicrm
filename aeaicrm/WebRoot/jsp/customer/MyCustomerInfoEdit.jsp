<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>客户信息</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (!ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('detail');
				$('#CUST_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			if(parent.PopupBox && parent.refreshContent){
				parent.refreshContent();
				parent.PopupBox.closeCurrent();
			}else{
				$('#operaType').val('update');
				$('#CUST_ID').val();
				doSubmit({actionType:'prepareDisplay'});
			}
		}
	}});	
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			$('#currentSubTableId').val(subTableId);
			postRequest('form1',{actionType:'deleteEntryRecord',onComplete:function(responseText){
				if (responseText != ''){
					jAlert(responseText);
				}else{
					doSubmit({actionType:'prepareDisplay'});
				}
			}});
		}
	});
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	if ("contactPerson"==subTableId){
		for (var i=0;i < currentRecordSize;i++){
		}
	}
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&CUST_ID='+$('#CUST_ID').val();
	insertSubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			doSubmit({actionType:'deleteSubRecord'});
		}
	});
}
function doMoveUp(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	doSubmit({actionType:'moveDown'});
}
function stateSubmit(){
	postRequest('form1',{actionType:'submit',onComplete:function(responseText){
		if (responseText == 'success'){
			if(parent.PopupBox && parent.refreshContent){
				parent.refreshContent();
				parent.PopupBox.closeCurrent();
			}else{
				$('#operaType').val('detail');
				$('#CUST_ID').val();
				doSubmit({actionType:'prepareDisplay'});
			}
		}
	}});
}
function stateSubmitCounter(){
	postRequest('form1',{actionType:'submitCounter',onComplete:function(responseText){
		if(parent.PopupBox && parent.refreshContent){
			parent.refreshContent();
			parent.PopupBox.closeCurrent();
		}else{
			$('#operaType').val('detail');
			$('#CUST_ID').val();
			doSubmit({actionType:'prepareDisplay'});
		}
	}});
}

function stateConfirm(){
	postRequest('form1',{actionType:'confirm',onComplete:function(responseText){
		if(parent.PopupBox && parent.refreshContent){
			parent.refreshContent();
			parent.PopupBox.closeCurrent();
		}else{
			$('#operaType').val('detail');
			$('#CUST_ID').val();
			doSubmit({actionType:'prepareDisplay'});
		}
	}});
}
function stateConfirmCounter(){
	postRequest('form1',{actionType:'confirmCounter',onComplete:function(responseText){
		if(parent.PopupBox && parent.refreshContent){
			parent.refreshContent();
			parent.PopupBox.closeCurrent();
		}else{
			$('#operaType').val('detail');
			$('#CUST_ID').val();
			doSubmit({actionType:'prepareDisplay'});
		}
	}});
}

var proIdBox;
function openProIdBox(){
	var handlerId = "ProvinceListSelectList"; 
	if (!proIdBox){
		proIdBox = new PopupBox('proIdBox','请选择省份      ',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=CUST_PROVINCE&targetName=CUST_PROVINCE_NAME';
	proIdBox.sendRequest(url);
}
var addUserTreeBox;
function showAddUserTreeBoxBox(){
	if (!addUserTreeBox){
		addUserTreeBox = new PopupBox('addUserTreeBox','请选择销售',{size:'normal',height:'410px',width:'310px',top:'3px'});
	}
	var handlerId = "CustomerSalesPersonnelInfoSelect";
	var url = 'index?'+handlerId+'&custId='+$("#custId").val();
	addUserTreeBox.sendRequest(url);
}
function addUserTreeRelation(userIds){
	if (userIds != ""){
		$('#userIds').val(userIds);
		postRequest('form1',{actionType:'addUserTreeRelation',onComplete:function(responseText){
			$("#salMans").html(responseText);
		}});
	}
}
function delUserTreeRelation(){
	if ($("#salMans").val() == null){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确定要删除该销售关联么？',function(r){
		if(r){
			postRequest('form1',{actionType:'delUserTreeRelation',onComplete:function(responseText){
				$("#salMans").html(responseText);
			}});	
		}
	});
}
function refreshCitySelect(){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=refreshCitySelect&CUST_PROVINCE="+$('#CUST_PROVINCE').val();
	sendRequest(url,{onComplete:function(responseText){
		$("#CUST_CITY").html(responseText);
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
 <div class="newarticle1" onclick="changeSubTable('visit')">拜访记录</div>
 <div class="newarticle1" onclick="changeSubTable('opportunity')">商机管理</div>
 <div class="newarticle1" onclick="changeSubTable('order')">订单管理</div>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<%
if("_base".equals(pageBean.inputValue("currentSubTableId"))||"contactPerson".equals(pageBean.inputValue("currentSubTableId"))){
%>
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
 <%if(pageBean.getBoolValue("doDetail")){%>
    <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
    <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <%}%>
  <%if(pageBean.getBoolValue("dosubmit")){%>
    <aeai:previlege code="submit"><td  align="center" class="bartdx" onclick="stateSubmit();" onmouseover="onMover(this);" onmouseout="onMout(this);"><input value="&nbsp;" type="button" class="submitImgBtn" id="submitImgBtn" title="提交" />提交</td></aeai:previlege>
   <%}%>
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="CUST_NAME" type="text" class="text" id="CUST_NAME" value="<%=pageBean.inputValue("CUST_NAME")%>" size="24" label="名称" />
    </td>
	<th width="100" nowrap>行业</th>
	<td colspan="3"><select id="CUST_INDUSTRY" label="行业" name="CUST_INDUSTRY" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_INDUSTRY")%></select>
</tr>
<tr>
	<th width="100" nowrap>省份城市</th>
	<td><input id="CUST_PROVINCE_NAME" name="CUST_PROVINCE_NAME" type="text" value="<%=pageBean.inputValue("CUST_PROVINCE_NAME")%>" size="24" class="text" readonly="readonly" /><input type="hidden" label="省份" id="CUST_PROVINCE" name="CUST_PROVINCE" value="<%=pageBean.inputValue("CUST_PROVINCE")%>" /><img id="proIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()" />
	<select id="CUST_CITY" label="城市" name="CUST_CITY" class="select" style="width:80px;">
	  <%=pageBean.selectValue("CUST_CITY")%>
	</select>
	</td>
  	<th width="100" nowrap>公司网站</th>
	<td colspan="3"><input name="CUST_COMPANY_WEB" type="text" class="text" id="CUST_COMPANY_WEB" value="<%=pageBean.inputValue("CUST_COMPANY_WEB")%>" size="24" label="名称" />
    </td>
</tr>
<tr>
	<th width="100" nowrap>规模</th>
	<td><select id="CUST_SCALE" label="规模" name="CUST_SCALE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_SCALE")%></select>
</td>
	<th width="100" nowrap>性质</th>
	<td><select id="CUST_NATURE" label="性质" name="CUST_NATURE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_NATURE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>级别</th>
	<td>
    <select id="CUST_LEVEL" label="级别" name="CUST_LEVEL" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_LEVEL")%></select>
</td>
	<th width="100" nowrap>进展状态</th>
	<td colspan="3">
    <select id="CUST_PROGRESS_STATE" label="进展状态" name="CUST_PROGRESS_STATE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_PROGRESS_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="CUST_CREATE_NAME" type="text" class="text" id="CUST_CREATE_NAME" value="<%=pageBean.inputValue("CUST_CREATE_NAME")%>" size="24" readonly="readonly"  label="创建人" />	  <input name="CUST_CREATE_ID" type="hidden" class="text" id="CUST_CREATE_ID" value="<%=pageBean.inputValue("CUST_CREATE_ID")%>" size="24" label="创建人" />
</td>
	<th width="100" nowrap>创建时间</th>
	<td colspan="3"><input name="CUST_CREATE_TIME" type="text" class="text" id="CUST_CREATE_TIME" value="<%=pageBean.inputTime("CUST_CREATE_TIME")%>" size="24" readonly="readonly" label="创建时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>提交人</th>
	<td><input name="CUST_SUBMIT_NAME" type="text" class="text" id="CUST_SUBMIT_NAME" value="<%=pageBean.inputValue("CUST_SUBMIT_NAME")%>" size="24" readonly="readonly"  label="创建人" />	
    <input id="CUST_SUBMIT_ID" label="提交人" name="CUST_SUBMIT_ID" type="hidden" value="<%=pageBean.inputValue("CUST_SUBMIT_ID")%>" size="24" class="text" />
</td>
	<th width="100" nowrap>提交时间</th>
	<td colspan="3"><input name="CUST_SUBMIT_TIME" type="text" class="text" id="CUST_SUBMIT_TIME" value="<%=pageBean.inputTime("CUST_SUBMIT_TIME")%>" size="24" readonly="readonly" label="提交时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>确认人</th>
	<td><input name="CUST_CONFIRM_NAME" type="text" class="text" id="CUST_CONFIRM_NAME" value="<%=pageBean.inputValue("CUST_CONFIRM_NAME")%>" size="24" readonly="readonly"  label="创建人" />
    <input name="CUST_CONFIRM_ID" type="hidden" class="text" id="CUST_CONFIRM_ID" value="<%=pageBean.inputValue("CUST_CONFIRM_ID")%>" size="24" label="确认人" />
</td>
	<th width="100" nowrap>确认时间</th>
	<td colspan="3"><input name="CUST_CONFIRM_TIME" type="text" class="text" id="CUST_CONFIRM_TIME" value="<%=pageBean.inputTime("CUST_CONFIRM_TIME")%>" size="24"  readonly="readonly" label="确认时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input id="CUST_ADDRESS" label="地址" name="CUST_ADDRESS" type="text" value="<%=pageBean.inputValue("CUST_ADDRESS")%>" style="width:504px;" size="65" class="text" />
</td>
	<th width="100" nowrap>状态</th>
	<td colspan="3">
    <input id="STATE_TEXT" label="状态" name="STATE_TEXT" type="text" value="<%=pageBean.selectedText("CUST_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="CUST_STATE" label="状态" name="CUST_STATE" type="hidden" value="<%=pageBean.selectedValue("CUST_STATE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>简要介绍</th>
	<td style="width:510px;"><textarea id="CUST_INTRODUCE" label="简要介绍" name="CUST_INTRODUCE" cols="60" rows="6" class="textarea" style="width:500px;"><%=pageBean.inputValue("CUST_INTRODUCE")%></textarea>
</td>
<%if (!"insert".equals(pageBean.getOperaType()) && (pageBean.getBoolValue("hasRight"))){%> 
	<th width="100" nowrap>关联销售</th>
	<td valign="top"><span style="float:left"><select name="salMans" size="5" id="salMans" class="xselect" style="width:213px;height: 96px;"><%=pageBean.selectValue("salMans")%>
      </select></span>
      <span><input name="addKeyWordRef" type="button" value="添加" style="margin:2px;" onclick="showAddUserTreeBoxBox()" />
      <br />
      <input name="delKeyWordRef" type="button" value="删除" style="margin:2px;" onclick="delUserTreeRelation()" /></span>
      </td>
      <%} %>
</tr>
</table>
</div>
<%} %>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<%-- <%if ("contactPerson".equals(currentSubTableId)){ %>  --%>
<%
// if("_base".equals(pageBean.inputValue("currentSubTableId"))||"contactPerson".equals(pageBean.inputValue("currentSubTableId"))){
%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
    <aeai:previlege code="createBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="addEntryRecord('contactPerson')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
    <aeai:previlege code="saveBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveMasterRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveSubImgBtn" title="保存" />保存</td></aeai:previlege>
    <aeai:previlege code="deleteBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteEntryRecord('contactPerson')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
    <aeai:previlege code="cancelBtn"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="refreshPage()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" />取消</td></aeai:previlege>
</tr>   
   </table>
</div>
<%-- <%} %> --%>
<div style="margin:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="30" align="center" nowrap="nowrap">序号</th>
	<th width="80" align="center">姓名</th>
	<th width="80" align="center">性别</th>
    <th width="80" align="center">职位</th>
	<th width="80" align="center">电话</th>
	<th width="80" align="center">邮箱</th>
	<th width="80" align="center">备注</th>
  </tr>
</thead>
<tbody>
<%
List paramRecords = (List)pageBean.getAttribute("contactPersonRecords");
pageBean.setRsList(paramRecords);
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
   <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%>
<input type="hidden" id="CONT_ID_<%=i%>" label="CONT_ID" name="CONT_ID_<%=i%>" value="<%=pageBean.inputValue(i,"CONT_ID")%>" />
<input type="hidden" id="CUST_ID_<%=i%>" label="CUST_ID" name="CUST_ID_<%=i%>" value="<%=pageBean.inputValue(i,"CUST_ID")%>" />
	<input id="state_<%=i%>" name="state_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" />
	</td>
	<td><input id="CONT_NAME_<%=i%>" label="姓名" name="CONT_NAME_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"CONT_NAME")%>" size="6"  class="text" />
</td>
	<td><select id="CONT_SEX_<%=i%>" label="性别" name="CONT_SEX_<%=i%>" class="select"><%=pageBean.selectValue(i,"CONT_SEX","CONT_SEXSelect")%></select>
</td>
<td><input id="CONT_JOB_<%=i%>" label="职位" name="CONT_JOB_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"CONT_JOB")%>" size="15" class="text" />
</td>
	<td><input id="CONT_PHONE_<%=i%>" label="电话" name="CONT_PHONE_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"CONT_PHONE")%>" size="20" class="text" />
</td>
	<td><input id="CONT_EMAIL_<%=i%>" label="邮箱" name="CONT_EMAIL_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"CONT_EMAIL")%>" size="30" class="text" />
</td>
	<td><input id="CONT_OTHER_<%=i%>" label="备注" name="CONT_OTHER_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"CONT_OTHER")%>" size="24" class="text" />
</td>
  </tr>
<%}%>
</tbody>  
</table>
</div>
</div>
<input type="hidden" id="currentRecordSize" name="currentRecordSize" value="<%=pageBean.listSize()%>" />
<input type="hidden" id="currentRecordIndex" name="currentRecordIndex" value="" />

<script language="javascript">
<%for (int i=0;i < paramSize;i++){%>
	$("input[id$='_<%=i%>'],select[id$='_<%=i%>']").change(function(){
		if ($("#state_<%=i%>").val()==""){
			$("#state_<%=i%>").val('update');
		}
	});
<%}%>
setRsIdTag('currentRecordIndex');
</script>

<%-- <%}%> --%>
<%}%>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;display:none;overflow:hidden;">
<%
if("visit".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?MyCustVisitManageList&custId=<%=pageBean.inputValue("CUST_ID")%>" width="100%" height="570" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;display:none;overflow:hidden;">
<%
if("opportunity".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?MyCustOppInfoManageList&custId=<%=pageBean.inputValue("CUST_ID")%>" width="100%" height="570" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer3" style="height:auto;display:none;overflow:hidden;">
<%
if("order".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?MyCustOrderInfoManageList&custId=<%=pageBean.inputValue("CUST_ID")%>" width="100%" height="570" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>

<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus({"id":"<%=pageBean.inputValue("currentLayerleId")%>"});
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="GRP_ID" id="GRP_ID" value="<%=pageBean.inputValue("GRP_ID")%>"/>
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" />
<input type="hidden" name="ROLE_ID" id="ROLE_ID" value="" />
<input type="hidden" name="USER_ID" id="USER_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<input type="hidden" id="custId" name="custId" value="<%=pageBean.inputValue("CUST_ID")%>" />
<input type="hidden" id="userIds" name="userIds" value="" />
<script language="JavaScript">
setRsIdTag('ROLE_ID,USER_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</div>
</form>
<script language="javascript">
$('#CUST_INTRODUCE').inputlimiter({
	limit: 300,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
datetimeValidators[0].set("yyyy-MM-dd HH:mm").add("CUST_CREATE_TIME");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("CUST_SUBMIT_TIME");
datetimeValidators[2].set("yyyy-MM-dd HH:mm").add("CUST_CONFIRM_TIME");
initDetailOpertionImage();
requiredValidator.add("CUST_NAME");
requiredValidator.add("CONT_NAME");
requiredValidator.add("CUST_LEVEL");
requiredValidator.add("CUST_PROGRESS_STATE");
new BlankTrimer("CUST_NAME");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
