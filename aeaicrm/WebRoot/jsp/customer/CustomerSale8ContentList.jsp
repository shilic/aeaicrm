<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if ('insert' != operaType && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',height:'600px',width:'1050',top:'3px',scroll:'yes'});
	}
	var columnIdValue = $("#curColumnId").val();
	if ('insert' == operaType){
		columnIdValue = $("#columnId").val();
	}
	var url = 'index?'+handlerId+'&GRP_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);	
}
function doRemoveContent(){
	bachProcessIds();
	var ids = $("#ids").val();
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要移除该条记录吗？',function(r){
		if(r){
			doSubmit({actionType:'deleteSaleRelation'});
		}
	});
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	bachProcessIds();
	var ids = $("#ids").val();
	var columnIdValue = $("#columnId").val();
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "CustomerSalePick";
	var url = 'index?'+handlerId+'&USER_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());		
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}
var custIdBox;
function openCustIdBox(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var handlerId = "CustomerSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择客户',{size:'normal',width:'800',top:'2px'});
	}
	var custId = $('#CUST_ID').val();
	var custName = $('#CUST_NAME').val();
	var url = 'index?'+handlerId+'&targetId=CUST_ID&CUST_ID='+custId+'&custName='+custName;
	custIdBox.sendRequest(url);
}
function doMergeRecord(custId,targetId){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=mergeRecord&custId="+custId+"&targetId="+targetId;
	sendRequest(url,{onComplete:function(responseText){
		if(responseText == "success"){
			jAlert('合并成功');
			doSubmit({actionType:'prepareDisplay'});
		}
	}});
}
function selectedCheck(indexId){	
	var idInt = parseInt(indexId);
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'CUST_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'CUST_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'CUST_ID']").attr('checked',true);
	}
}
function bachProcessIds(){
	var ids = "";
	$("input:[name = 'CUST_ID'][checked]").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	$("#ids").val(ids);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">
    	<a href="index?CustomerSale8ContentList" style="color:#FFEB3B">&nbsp;&nbsp;销售人员</a>
    	<a href="index?CustomerGroup8ContentList">&nbsp;&nbsp;分组列表</a>
    </h3>   
	
    <div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
	<td width="85%" valign="top">
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="openContentRequestBox('insert','客户信息','CustomerInfoEdit','CUST_ID')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="openContentRequestBox('update','客户信息','CustomerInfoEdit','CUST_ID')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="copy"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="openContentRequestBox('copy','客户信息','CustomerInfoEdit','CUST_ID')"><input id="copyImgBtn" value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td></aeai:previlege>
   <%if(pageBean.getBoolValue("hasRight")){%>
   <aeai:previlege code="confirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="openContentRequestBox('confirm','客户信息','CustomerInfoEdit','CUST_ID')"><input id="confirm" value="&nbsp;" title="确认" type="button" class="confirmImgBtn" />确认</td></aeai:previlege>
   <aeai:previlege code="reConfirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="F" align="center" onclick="openContentRequestBox('confirmCounter','客户信息','CustomerInfoEdit','CUST_ID')"><input id="confirmCounter" value="&nbsp;" title="反确认" type="button" class="revokeConfirmImgBtn" />反确认</td></aeai:previlege>
   <%}%>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="openContentRequestBox('detail','客户信息','CustomerInfoEdit','CUST_ID')"><input id="detailImgBtn" value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege>
   <aeai:previlege code="filter"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="F" align="center" onclick="showFilterBox()"><input value="&nbsp;" title="过滤" type="button" class="filterImgBtn" />过滤</td></aeai:previlege>
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delete" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
   <aeai:previlege code="remove"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="doRemoveContent()"><input value="&nbsp;" title="移除" type="button" class="removeImgBtn" id="remove" />移除</td></aeai:previlege>
   <aeai:previlege code="transfer"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openTargetTreeBox('moveContent')"><input value="&nbsp;" title="迁移" type="button" class="moveImgBtn" />迁移</td></aeai:previlege>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openCustIdBox()"><input value="&nbsp;" title="合并记录" type="button" class="relateImgBtn" />合并记录</td>
</tr>
</table>
</div>
<div id="rightArea">
<ec:table 
form="form1"
var="row"
useAjax="true" sortable="true"
items="pageBean.rsList"
retrieveRowsCallback="process"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();openContentRequestBox('detail','客户信息','CustomerInfoEdit','CUST_ID')" oncontextmenu="selectRow(this,{CUST_ID:'${row.CUST_ID}',curColumnId:'${row.GRP_ID}'});controlUpdateBtn('${row.CUST_STATE}');refreshConextmenu()" onclick="selectedCheck('${GLOBALROWCOUNT}');selectRow(this,{CUST_ID:'${row.CUST_ID}',curColumnId:'${row.GRP_ID}',CUST_NAME:'${row.CUST_NAME}'});controlUpdateBtn('${row.CUST_STATE}');">
	<ec:column width="25" style="text-align:center" property="CUST_ID" cell="checkbox" headerCell="checkbox" onclick="selectedCheck('${GLOBALROWCOUNT}');"/>
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_NAME" title="名称"   />
    <ec:column width="100" property="CUST_PROVINCE_NAME" title="省份"   />
	<ec:column width="100" property="CUST_CITY_NAME" title="城市"   />
	<ec:column width="100" property="CUST_INDUSTRY" title="行业"   mappingItem="CUST_INDUSTRY"/>
	<ec:column width="100" property="CUST_SCALE" title="规模"   mappingItem="CUST_SCALE"/>
	<ec:column width="100" property="CUST_LEVEL" title="级别"   mappingItem="CUST_LEVEL"/>
	<ec:column width="100" style="text-align:right" property="VISIT_NUM" title="拜访数"  />
	<ec:column width="100" style="text-align:right" property="OPP_NUM" title="商机数" />
	<ec:column width="100" property="CUST_STATE" title="状态"   mappingItem="CUST_STATE"/>
</ec:row>
</ec:table>

<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;display:none; z-index:10; width:480px;">
<b class="b9"></b>
<div class="content">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="custName" label="名称" name="custName" type="text" value="<%=pageBean.inputValue("custName")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>行业</th>
	<td><select id="custIndustry" label="行业" name="custIndustry" class="select"><%=pageBean.selectValue("custIndustry")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>规模</th>
	<td><select id="custScale" label="规模" name="custScale" class="select"><%=pageBean.selectValue("custScale")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>级别</th>
	<td><%=pageBean.selectCheckBox("custLevel")%></td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><%=pageBean.selectCheckBox("custState")%></td>
</tr>
<tr>
<th width="100" nowrap>省份</th>
	<td><input id="custProvinceName" name="custProvinceName" type="text" value="<%=pageBean.inputValue("custProvinceName")%>" size="24" class="text" readonly="readonly" />
    <input type="hidden" label="省份" id="CUST_PROVINCE" name="CUST_PROVINCE" value="<%=pageBean.inputValue("custProvince")%>" /><img id="proIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()" />
</td>
</tr>
<tr>
	<th width="100" nowrap>城市</th>
	<td><select id="custCity" label="城市" name="custCity" class="select"><%=pageBean.selectValue("custCity")%></select>
</td>
</tr>
</table>
<div style="width:100%;text-align:center;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="CUST_ID" id="CUST_ID" value="" />
<input type="hidden" name="CUST_NAME" id="CUST_NAME" value="" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<input type="hidden" id="ids" name="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
function refreshCitySelect(){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=refreshCitySelect&CUST_PROVINCE="+$('#CUST_PROVINCE').val();
	sendRequest(url,{onComplete:function(responseText){
		//jAlert(responseText);	
		//eval(responseText);
		$("#custCity").html(responseText);
	}});
}
setRsIdTag('CUST_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
$(function(){
	resetTreeHeight(80);	
	resetRightAreaHeight(87);
});
function controlUpdateBtn(stateResult){
	if(stateResult =='init'){
		enableButton("editImgBtn");
		disableButton("confirm");
		enableButton("detailImgBtn");
		enableButton("delete");
		enableButton("remove");
		disableButton("confirmCounter");
	}else if(stateResult =='Submit'){
		disableButton("editImgBtn");
		enableButton("confirm");
		enableButton("detailImgBtn");
		disableButton("delete");
		disableButton("remove");
		disableButton("confirmCounter");
	}else{
		disableButton("editImgBtn");
		disableButton("confirm");
		enableButton("detailImgBtn");
		disableButton("delete");
		disableButton("remove");
		enableButton("confirmCounter");
	}	
}
var proIdBox;
function openProIdBox(){
	var handlerId = "ProvinceListSelectList"; 
	if (!proIdBox){
		proIdBox = new PopupBox('proIdBox','请选择省份      ',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=CUST_PROVINCE&targetName=custProvinceName';
	proIdBox.sendRequest(url);
}
</script>
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
