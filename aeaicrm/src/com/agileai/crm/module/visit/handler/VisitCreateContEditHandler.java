package com.agileai.crm.module.visit.handler;

import com.agileai.crm.cxmodule.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class VisitCreateContEditHandler extends StandardEditHandler{
	public VisitCreateContEditHandler() {
		super();
		this.serviceId = buildServiceId(VisitManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		setAttribute("CUST_ID",param.get("custId"));
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected void processPageAttributes(DataParam param) {
		
	    this.setAttribute("CONT_SEX",
                FormSelectFactory.create("USER_SEX")
                                 .addSelectedValue(getAttributeValue("CONT_SEX",
                                                                          "M")));
	}
	@PageAction
	public ViewRenderer createCont(DataParam param) {
		param.put("CUST_ID", param.get("CUST_ID").substring(0, 36));
		((VisitManage) getService()).createContRecord(param);
		String responseText = param.get("CONT_ID");
		return new AjaxRenderer(responseText);
	}
	protected StandardService getService() {
		return (StandardService) this.lookupService(this.getServiceId());
	}
}
