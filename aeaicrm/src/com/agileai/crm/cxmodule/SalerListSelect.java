package com.agileai.crm.cxmodule;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelService;

public interface SalerListSelect
        extends PickFillModelService {
	public List<DataRow> querySaleListRecords(); 
}
